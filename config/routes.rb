Rails.application.routes.draw do
  devise_for :users

  resources :inspections do
    collection do
      get '/realtors', action: 'realtors'
      get '/marketing', action: 'marketing'
    end
    resources :agreements, only: [:new, :create, :edit, :update]
  end
  resources :agreements, only: [:index, :show]

  resources :users do
    collection do
      post '/create_inspector', action: 'create_inspector'
    end
  end

  root to: "pages#index"
end
