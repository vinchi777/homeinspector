namespace :permission do
  task :change do
    on roles(:app) do
      execute "chmod g+x,o+x #{release_path}/config.ru"
    end
  end
end
