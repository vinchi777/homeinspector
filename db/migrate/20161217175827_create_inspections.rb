class CreateInspections < ActiveRecord::Migration[5.0]
  def change
    create_table :inspections do |t|
      t.string :room_type
      t.string :inspection_type
      t.string :property_code
      t.string :address

      t.string :buyers_name
      t.string :buyers_phone
      t.string :buyers_email

      t.string :realtors_name
      t.string :realtors_phone
      t.string :realtors_email

      t.timestamps
    end
  end
end
