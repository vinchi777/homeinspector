class ChangeRoomTypeOnInspections < ActiveRecord::Migration[5.0]
  def change
    rename_column :inspections, :room_type, :report_type
  end
end
