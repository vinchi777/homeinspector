class CreateAgreements < ActiveRecord::Migration[5.0]
  def change
    create_table :agreements do |t|
      t.string :user_type
      t.string :email

      t.timestamps
    end
  end
end
