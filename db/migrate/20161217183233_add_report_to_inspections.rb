class AddReportToInspections < ActiveRecord::Migration[5.0]
  def change
    add_column :inspections, :report, :string
  end
end
