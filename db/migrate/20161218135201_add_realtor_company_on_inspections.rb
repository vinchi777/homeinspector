class AddRealtorCompanyOnInspections < ActiveRecord::Migration[5.0]
  def change
    add_column :inspections, :realtors_company, :string
  end
end
