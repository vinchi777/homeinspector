class AddFieldsOnInspections < ActiveRecord::Migration[5.0]
  def change
    add_column :inspections, :marketing_source, :string
    add_column :inspections, :paying_at_closing, :boolean
    add_column :inspections, :estimated_closing_date, :date
    add_column :inspections, :inspection_fee, :integer
    add_column :inspections, :email_option, :string
  end
end
