class AddForeignKeyOnAgreements < ActiveRecord::Migration[5.0]
  def change
    add_reference :agreements, :inspection, index: true
    add_reference :inspections, :user, index: true
  end
end
