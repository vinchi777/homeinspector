class ChangeDefaultInspectionFee < ActiveRecord::Migration[5.0]
  def change
    change_column :inspections, :inspection_fee, :integer, :default => 0
  end
end
