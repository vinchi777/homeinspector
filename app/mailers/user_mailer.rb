class UserMailer < ApplicationMailer
  default from: "don.nbhi@gmail.com"

  def notify_inspection(inspection, recipient)
    @inspection = inspection
    @user_type = recipient
    @sender = @inspection.user.email

    if recipient == 'buyer'
      @email = @inspection.buyers_email
      @name = @inspection.buyers_name
    elsif recipient == 'realtor'
      @email = @inspection.realtors_email
      @name = @inspection.realtors_name
    end

    mail(to: [@email, @sender], subject: 'NBHI Home Inspection Report', from: @sender)
  end
end
