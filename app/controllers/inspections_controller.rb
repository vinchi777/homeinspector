class InspectionsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_inspection, only: [:show, :edit, :update, :destroy]

  # GET /inspections
  # GET /inspections.json
  def index
    if params[:from].present?
      @inspections = collection.where('created_at > ? ', params[:from])
    else
      @inspections = collection
    end
  end

  # GET /inspections/1
  # GET /inspections/1.json
  def show
    redirect_to @inspection.report.url
  end

  # GET /inspections/new
  def new
    @inspection = Inspection.new
  end

  # GET /inspections/1/edit
  def edit
  end

  # POST /inspections
  # POST /inspections.json
  def create
    @inspection = current_user.inspections.new(inspection_params)

    respond_to do |format|
      if @inspection.save
        @inspection.email_inspection

        format.html { redirect_to inspections_path, notice: 'Inspection was successfully created.' }
        format.json { render :show, status: :created, location: @inspection }
      else
        format.html { render :new }
        format.json { render json: @inspection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inspections/1
  # PATCH/PUT /inspections/1.json
  def update
    respond_to do |format|
      if @inspection.update(inspection_params)
        format.html { redirect_to @inspection, notice: 'Inspection was successfully updated.' }
        format.json { render :show, status: :ok, location: @inspection }
      else
        format.html { render :edit }
        format.json { render json: @inspection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inspections/1
  # DELETE /inspections/1.json
  def destroy
    @inspection.destroy
    respond_to do |format|
      format.html { redirect_to inspections_url, notice: 'Inspection was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def realtors
    @realtors = {}
    collection.each do |i|
      next if i.realtors_name.nil?

      @realtors[i.realtors_name] ||= { count: 0, last_referal: nil }
      @realtors[i.realtors_name][:count] += 1
      @realtors[i.realtors_name][:last_referal] = (i.created_at.to_i > @realtors[i.realtors_name][:last_referal].to_i) ? i.created_at : @realtors[i.realtors_name][:last_referal]
    end
  end

  def marketing
    @monthly_inspections = {}
    @inspections = Inspection.where('created_at >= ?', Date.today.beginning_of_year).group_by { |i| i.created_at.to_date.beginning_of_month.to_s }

    months_of_year.each do |month|
      inspections_of_the_month = @inspections[month] || []
      @monthly_inspections[month] = {count: inspections_of_the_month.count, revenue: inspections_of_the_month.map(&:inspection_fee).sum}
    end
  end

  private

    def collection
      current_user.has_role?(:inspector) ? current_user.inspections : Inspection.all
    end

    def months_of_year
      (Date.today.beginning_of_year..Date.today.end_of_year).map(&:beginning_of_month).uniq.map(&:to_s)
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_inspection
      @inspection = Inspection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inspection_params
      params.require(:inspection).permit(
        :report_type,
        :inspection_type,
        :property_code,
        :address,
        :buyers_name,
        :buyers_phone,
        :buyers_email,
        :realtors_name,
        :realtors_phone,
        :realtors_email,
        :realtors_company,
        :report,
        :marketing_source,
        :paying_at_closing,
        :estimated_closing_date,
        :inspection_fee,
        email_option: []
      )
    end
end
