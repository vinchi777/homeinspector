class AgreementsController < ApplicationController
  before_action :authenticate_user!, only: [:index]

  def new
    @inspection = Inspection.find(params[:inspection_id])
    @agreement = @inspection.agreements.new(
      user_type: params[:user_type],
      email: params[:email]
    )

    render :layout => 'guest'
  end

  def create
    @inspection = Inspection.find(params[:inspection_id])
    @agreement = @inspection.agreements.new(agreement_params)

    success = @inspection.agreements.where(:email => @agreement.email).exists? ? true : @agreement.save

    respond_to do |format|
      if (success && @agreement.valid?)
        format.html { redirect_to @inspection, notice: 'Inspection was successfully created.' }
        format.json { render :show, status: :created, location: @agreement }
      else
        format.html { render :new, layout: 'guest' }
        format.json { render json: @agreement.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    @agreements = collection
  end


  private

    def collection
      current_user.has_role?(:inspector) ? Agreement.joins(:inspection).where('inspections.user_id = ?', current_user.id) : Agreement.all
    end

    def agreement_params
      params.require(:agreement).permit(
        :terms,
        :email,
        :user_type,
      )
    end

end
