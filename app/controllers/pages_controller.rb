class PagesController < ApplicationController
  before_action :authenticate_user!

  def index
    @inspections = collection.group_by { |i| i.created_at.beginning_of_month }
  end

  private

  def collection
    current_user.has_role?(:inspector) ? current_user.inspections : Inspection.all
  end

end
