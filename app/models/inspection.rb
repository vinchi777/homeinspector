class Inspection < ApplicationRecord
  mount_uploader :report, ReportUploader
  serialize :email_option, Array

  has_many :agreements, dependent: :destroy
  belongs_to :user

  EMAIL_OPTIONS = ['buyer', 'realtor', 'none']

  def email_inspection
    self.email_option.each do |recipient|
      UserMailer.notify_inspection(self, recipient).deliver
    end
  end
end
