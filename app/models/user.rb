class User < ApplicationRecord

  has_many :inspections

  after_create :assign_inspector_role

  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def assign_inspector_role
    self.add_role(:inspector) if self.roles.blank?
  end

end
