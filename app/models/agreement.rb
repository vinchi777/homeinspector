class Agreement < ApplicationRecord
  belongs_to :inspection

  validates_acceptance_of :terms
  validates :inspection, presence: true
end
