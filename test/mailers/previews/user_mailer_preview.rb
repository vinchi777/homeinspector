# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def notify_inspection_preview
    UserMailer.notify_inspection(Inspection.first, 'buyer')
  end

end
